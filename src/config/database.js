const { Sequelize } = require('sequelize');

const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASS,
  {
    host: process.env.DB_HOST,
    port: 5432,
    dialect: 'postgres',
    logging: console.log,
  }
);

sequelize.authenticate()
  .then(async () => {
    console.log("Database connected")
    await sequelize.sync()
    console.log('synced')
  })
  .catch((e) => console.log("Error:" + e));

module.exports = sequelize;
