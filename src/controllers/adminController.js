const Bet = require('../models/bet');
const User = require('../models/user');
const Event = require('../models/event');

exports.createEvent = async (req, res) => {
  const { eventName, odds } = req.body;
  const event = await Event.create({ eventName, odds });
  res.json(event);
};

exports.getBetsForEvent = async (req, res) => {
  const { eventId } = req.params;
  const bets = await Bet.findAll({ where: { eventId } });
  res.json(bets);
};

exports.setEventResult = async (req, res) => {
  const { eventId, result } = req.body;
  const event = await Event.findByPk(eventId);
  event.result = result;
  await event.save();
  res.json(event);
};

exports.payoutWinners = async (req, res) => {
  const { eventId } = req.params;
  const event = await Event.findByPk(eventId);
  const bets = await Bet.findAll({ where: { eventId } });
  const payouts = [];

  for (let bet of bets) {
    if (bet.result === event.result) {
      const user = await User.findOrCreateUser(bet.userId);
      const payout = bet.amount * event.odds;
      user.balance += payout;
      await user.save();
      payouts.push({ userId: user.id, amount: payout });
    }
  }

  res.json({ eventId, payouts });
};

exports.getUserStatistics = async (req, res) => {
  const users = await User.findAll();
  res.json(users);
};

exports.updateUserBalance = async (req, res) => {
  const { userId, amount } = req.body;
  const user = await User.findOrCreateUser(userId);
  user.balance = amount;
  await user.save();
  res.json(user);
};
