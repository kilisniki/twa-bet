const Bet = require('../models/bet');
const User = require('../models/user');

exports.getAvailableBets = async (req, res) => {
  const bets = await Bet.findAll();
  res.json(bets);
};

exports.placeBet = async (req, res) => {
  const { userId, eventId, amount } = req.body;
  const bet = await Bet.create({ userId, eventId, amount });
  res.json(bet);
};

exports.getBetHistory = async (req, res) => {
  const { userId } = req.params;
  const bets = await Bet.findAll({ where: { userId } });
  res.json(bets);
};

exports.getBalance = async (req, res) => {
  const { userId } = req.params;
  const [user] = await User.findOrCreateUser(userId);
  res.json({ balance: user.balance });
};

exports.rechargeBalance = async (req, res) => {
  const { userId, amount } = req.body;
  const [user] = await User.findOrCreateUser(userId);
  user.balance += Number(amount);
  await user.save();
  res.json({ balance: user.balance });
};

