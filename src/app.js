const express = require('express');
const bodyParser = require('body-parser');
const betRoutes = require('./routes/betRoutes');
const adminRoutes = require('./routes/adminRoutes');
const guiRoutes = require('./routes/guiRoutes');
const cors = require('cors')


const app = express();
app.use(cors())
app.use(bodyParser.json());

app.use('/api', betRoutes);
app.use('/api', adminRoutes);
app.use('/', guiRoutes);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
