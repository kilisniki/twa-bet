const express = require('express');
const adminController = require('../controllers/adminController');
const router = express.Router();

// API routes
router.post('/admin/event', adminController.createEvent);
router.get('/admin/event/:eventId/bets', adminController.getBetsForEvent);
router.post('/admin/event/:eventId/result', adminController.setEventResult);
router.post('/admin/event/:eventId/payout', adminController.payoutWinners);
router.get('/admin/users', adminController.getUserStatistics);
router.post('/admin/user/:userId/balance', adminController.updateUserBalance);

module.exports = router;
