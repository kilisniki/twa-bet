const express = require('express');
const betController = require('../controllers/betController');
const router = express.Router();

router.get('/bets', betController.getAvailableBets);
router.post('/bet', betController.placeBet);
router.get('/bet/history/:userId', betController.getBetHistory);
router.get('/balance/:userId', betController.getBalance);
router.post('/balance/recharge', betController.rechargeBalance);

module.exports = router;
