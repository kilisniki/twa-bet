const express = require('express');
const guiController = require('../controllers/guiController');
const path = require("path");
const router = express.Router();

// Serve admin panel
router.get('/admin', (req, res) => {
  res.sendFile(path.join(__dirname, '../views/admin.html'));
});

router.get('/', guiController.renderHomePage);

module.exports = router;
