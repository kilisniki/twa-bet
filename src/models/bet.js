const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Bet = sequelize.define('Bet', {
  userId: DataTypes.INTEGER,
  eventId: DataTypes.INTEGER,
  amount: DataTypes.FLOAT,
  odds: DataTypes.FLOAT,
  result: DataTypes.STRING,
});

module.exports = Bet;
