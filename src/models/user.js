const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const User = sequelize.define('User', {
  username: DataTypes.STRING,
  balance: DataTypes.FLOAT,
});

/**
 * Функция поиска или создания пользователя по username
 * @param {string} username - Имя пользователя для поиска или создания
 * @returns {Promise<[User, boolean]>} - Возвращает массив, где первый элемент - объект пользователя, второй элемент - булево значение (true, если пользователь был создан, и false, если пользователь уже существовал)
 */
User.findOrCreateUser = async function (username) {
  try {
    const [user, created] = await User.findOrCreate({
      where: { username },
      defaults: { balance: 100 }
    });
    return [user, created];
  } catch (error) {
    console.error('Error finding or creating user:', error);
    throw error;
  }
}

module.exports = User;
