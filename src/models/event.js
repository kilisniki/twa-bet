const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Event = sequelize.define('Event', {
  eventName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  odds: {
    type: DataTypes.FLOAT,
    allowNull: false
  },
  result: {
    type: DataTypes.STRING,
    allowNull: true
  }
});

module.exports = Event;
